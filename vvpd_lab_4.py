import doctest


def check_length_or_radius(length_or_radius):
    if length_or_radius.isdigit():
        if int(length_or_radius) > 0:
            return True
    return False


def light(length, radius):
    length -= radius
    count = 1
    while length > radius:
        count += 1
        length -= 2 * radius
    return count


def main():
    while True:
        L = input('enter length of a road: ')
        if check_length_or_radius(L):
            L = int(L)
            break
        else:
            print('error')
            continue
    while True:
        R = input('enter light radius: ')
        if check_length_or_radius(R):
            R = int(R)
            break
        else:
            print('error')
            continue
    D = light(L, R)
    print(D)


if __name__ == '__main__':
    main()


def test_check_length_1():
    assert check_length_or_radius(10) == True


def test_check_length_2():
    assert check_length_or_radius(-10) == False


def test_check_length_3():
    assert check_length_or_radius('asdwas') == False


def test_check_radius_1():
    assert check_length_or_radius(10) == True


def test_check_radius_2():
    assert check_length_or_radius(-10) == False


def test_check_radius_3():
    assert check_length_or_radius('asdwas') == False


def test_light_1():
    assert light(5, 2) == 2


def test_light_2():
    assert light(1, 5) == 1


doctest.testmod()